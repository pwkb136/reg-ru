const isLoading = state => state.app.isLoading
const getTiles = state => state.app.tiles
const currentTile = state => state.app.currentTile


export default {
  isLoading,
  getTiles,
  currentTile
}

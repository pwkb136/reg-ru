import * as types from '../mutation-types'

const state = {
  isLoading: false,
  currentTile: {},
  tiles: []
}

const mutations = {
  [types.TOOGLE_LOADING] (state, isLoading) {
    state.isLoading = !!isLoading
  },
  [types.SET_TILES] (state, tiles) {
    state.tiles = tiles;
  },
  [types.SET_CURRENT_TILE] (state, tile) {
    state.currentTile = tile;
  }  
}

const actions = {
  toggleLoading ({ commit }, isLoading) {
    commit(types.TOOGLE_LOADING, isLoading)
  },
  setTiles ({ commit }, tiles) {
    commit(types.SET_TILES, tiles)
  },
  setCurrentTile ({ commit }, tile) {
    commit(types.SET_CURRENT_TILE, tile)
  },  
}

export default {
  state,
  mutations,
  actions
}

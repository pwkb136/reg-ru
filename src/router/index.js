import Vue from 'vue'
import Router from 'vue-router'
import lazyLoading from './lazyLoading'

Vue.use(Router);

const routes = [
  { 
    name: 'home', 
    path: '/home', 
    component: lazyLoading('home/Home'),
    meta: {
      title: 'Home'
    }
  },
  { 
    name: 'tileView', 
    path: '/details/:tileId?', 
    component: lazyLoading('tileView/TileView'),
    meta: {
      title: 'View tile details'
    }
  },  
  { 
    name: 'about', 
    path: '/about', 
    component: lazyLoading('about/About'),
    meta: {
      title: 'About'
    }
  },
  { 
    name: 'whatever', 
    path: '/whatever', 
    component: lazyLoading('whatever/Whatever'),
    meta: {
      title: 'What ever'
    }
  },
  { 
    path: '*',
    redirect: {
      name: 'home'
    }
  },  
];

const router = new Router({
  routes
}); 

router.beforeEach((to, from, next) => {
  const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

  if (nearestWithTitle) {
    document.title = nearestWithTitle.meta.title;
  }

  next();
});

export default router;
